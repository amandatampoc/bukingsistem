const express = require('express')
const Course = require('./../models/course')
const router = express.Router()

router.get('/new', (req, res) => {
  res.render('courses/new', { course: new Course() })
})

router.get('/edit/:id', async (req, res) => {
  const course = await Course.findById(req.params.id)
  res.render('courses/edit', { course: course })
})

router.get('/:slug', async (req, res) => {
  const course = await Course.findOne({ slug: req.params.slug })
  if (course == null) res.redirect('/')
  res.render('courses/show', { course: course })
})

router.post('/', async (req, res, next) => {
  req.course = new Course()
  next()
}, saveCourseAndRedirect('new'))

router.put('/:id', async (req, res, next) => {
  req.course = await Course.findById(req.params.id)
  next()
}, saveCourseAndRedirect('edit'))

router.delete('/:id', async (req, res) => {
  await Course.findByIdAndDelete(req.params.id)
  res.redirect('/courses')
})

function saveCourseAndRedirect(path) {
  return async (req, res) => {
    let course = req.course
    course.title = req.body.title
    course.description = req.body.description
    course.price = req.body.price
    course.enrollees = req.body.enrollees
    course.markdown = req.body.markdown
    try {
      course = await course.save()
      res.redirect(`/courses/${course.slug}`)
    } catch (e) {
      res.render(`courses/${path}`, { course: course })
    }
  }
}

module.exports = router